
// rekurzív függvény

function alma(x) { // execution context tartalma: {x, y}
    console.log(`alma(${x})`);
    var y = "alma" + x;
    x++; // ez csak az aktuális execution context x változójának értékét növeli
    if (x < 5) {
        alma(x); // a meghívás egy új execution contextet hoz létre, új x értékkel
        console.log(`after running alma(${x})`);
        console.log(`y = ${y}`);
    }
}

alma(1); // a meghívás egy új execution contextet hoz létre, új x=1 értékkel

// magyarázat angol nyelven: http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/