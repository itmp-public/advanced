
function hello(name) {
    console.log("Hello hivodott, argumentumok: ", arguments);
    return `${this.greeting}, dear ${name}!`;
}

console.log(hello("Peter"));

let myOb = {
    greeting : "Szervusz",
    greet : hello
};

console.log(myOb.greet("Jozsi"));


// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call
console.log(hello.call({ greeting: "Hi"}, "Jozsi", "Tibor"));
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply
console.log(hello.apply({greeting: "Szevasz"}, ["Feri", "Orsi"]));