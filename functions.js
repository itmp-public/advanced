// https://developer.mozilla.org/hu/docs/Web/JavaScript/Reference/Functions/rest_parameters

function sum(msg, ...numbers) {
    console.log(`Uzenet: ${msg}`, numbers);
    let sum = 0;
    for(let arg of numbers) {
        console.log(arg);
        sum += arg;
    }
    return sum;
}

let sum2 = (...numbers) => {
    console.log(numbers);
}

let x = sum("hello",2,8,9,9);
console.log(x);

sum2(3,3,4);