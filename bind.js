// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind

function placeSign(x, y, sign) {
    console.log(`placeSign hivodott, argumentumok: ${x}, ${y}, ${sign}`, "this", this);
}

let p12 = placeSign.bind({ ize: "hehe"}, 1,2);
let p20 = placeSign.bind({ ize: "haha"}, 2,0);

p12("X");
p20("O");

function hello(name) {
    console.log("Hello hivodott, argumentumok: ", arguments);
    return `${this.greeting}, dear ${name}!`;
}

let hi = hello.bind({greeting : "Hi"});

let howdy = hello.bind({greeting : "Howdy"});

console.log("hi(Peter)", hi("Peter"));

console.log("howdy(Peter)", howdy("Peter"));

// ez a myBind nevű függvény azt próbálja szemléltetni, hogy miként lehetne
// "házilag" implementálni a bind-ot:

function myBind(func, newThis, ...boundArgs) {
    // ez az execution context tartalmazza a newThis és a boundArgs értékét
    // az alább definiált function bezárja (closure) eme execution context-et
    // és így a kapott értékeket
    return function(...rest) {
        // a boundArgs hordozza az előre lefixált paramétereket egy tömb formájában
        // a rest paraméter már csak a nem előre lefixált paramétereket tartalmazza
        // ezt a két listát olvasztjuk össze:
        let newArguments = [...boundArgs, ...rest];
        func.apply(newThis, newArguments)
    };
}

let p22 = myBind(placeSign, { ize: "höhö"}, 2,2);

p22('X');