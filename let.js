// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let

function alma() {
    let arr = [];

    for(let i = 0; i < 5; i++) {
        arr.push(function() { console.log(`i = ${i}`)});
    }
    return arr;
}

let arr = alma();

arr[0]();
arr[1]();
arr[2]();
arr[3]();