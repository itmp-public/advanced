
function Task(content, done) {
    this.content = content;
    this.done = done;
}

function Feladat() {
}

Task.prototype.markDone = function() {
    this.done = true;
    console.log("markDone runs in prototype");
};

Feladat.prototype.hello = "hello Peter";

console.log(Feladat.prototype);
console.log(Task.prototype);

let t1 = new Task('levinni a szemetet', false);

console.log(Object.getPrototypeOf(t1) === Task.prototype);

console.log(t1 instanceof Object);

let o1 = new Object();

console.log(Object.getPrototypeOf(o1) === Object.prototype);

console.log(Array.prototype.constructor === Array);

