let game = {
    started : false,
    start() {
        this.started = true;
    }
};

function stop() {
    console.log("stop is running, this:", this);
    this.started = false;
}

console.log(game.started);
game.start();
console.log(game.started);
this.stop();
console.log(game.started);
game.stopGame = stop;

console.log(game);

game.stopGame();

console.log(game.started);
