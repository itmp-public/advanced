// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures

function alma() { // első execution context
    let arr = []; 

    for(var i = 0; i < 5; i++) {
        (function () { // IIFE: 
            var j = i; // második execution context
        arr.push(function() { console.log(`i = ${i}, j = ${j}`); /* harmadik exection context */});
        })();
    }
    return arr;
}

let arr = alma();

arr[0]();
arr[1]();
arr[2]();
arr[3]();