let str = 'Hello world!';
console.log(str.length);
console.log(str[10]);
console.log(str.includes('world',7));
console.log(str.indexOf('world', 7));
console.log(str.slice(6,11));
console.log(str.substr(6, 5));
console.log(str.substring(6, 11));
console.log(str.split("").reverse().join(""));
let longStr = 'Hello world! Hi Peter! Hi Adam!'
console.log(longStr.replace('Hi', 'Hello'));
console.log(longStr);
let replacedString = longStr;
while(replacedString.includes('Hi')){
    replacedString = replacedString.replace('Hi', 'Hello');
}
console.log(replacedString);
console.log(longStr.toLowerCase().replace(/Hi \w*/gi, 'Hello World'));