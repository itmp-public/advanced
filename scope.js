
function hello(name) {
    var msg = `Hello, dear ${name}`;

    function greet() {
        return msg; // hivatkozhatunk a külső execution contextek bármelyikében levő változókra
    }

    return greet; // ez a függvény referencia bezárja (tartalmazza) a külső execution context-et és annak szülőit: ezt nevezik closure-nek.  
}


var g1 = hello("Peter");
var g2 = hello("Jozsi");

console.log("g1()", g1());
console.log("g2()", g2());
