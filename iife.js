// https://developer.mozilla.org/en-US/docs/Glossary/IIFE

let stack = (function() {
    
    var arr = [];
    
    return {
        push(value) {
            arr.push(value);
        },
        pop() {
            return arr.pop();
        }
    };
})();

console.log(stack);
stack.push("AAAA");
stack.push("BBBB");
console.log(stack);
console.log(stack.pop());
console.log(stack.pop());
