class Task {
    constructor(content, done) {
        this.content = content;
        this.done = done;
    }

    markDone() {
        this.done = true;
        console.log("markDone runs in Task");
    }
}

class FinishedTask extends Task {
    constructor(content) {
        super(content, true);
    }

    markDone() {
        console.log("markDone runs in FinsihedTask");
    }

}

let t1 = new Task('hello', false);

let f1 = new FinishedTask('befejezett');

console.log(Object.getPrototypeOf(t1));
console.log(Object.getPrototypeOf(f1));

f1.markDone();

console.log(f1);

class MyArray extends Array {
    constructor() {
        super();
    }

    sum() {
        console.log("sum is running");
    }
}

let arr = new MyArray();

arr.push(12);

arr.sum();

console.log(arr);

console.log( arr instanceof Array);

