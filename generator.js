// 1 1 2 3 5 8 

function fibonacci(num) {
    if (num < 0) {
        throw new Error('Csak pozítiv számmal');
    }
    if (num >= 0 && num < 2) {
        return num
    }
    let a = 0;
    let b = 1;
    let result = 0;
    for(let i=1; i<num; i++){
        result = a + b;
        a = b;
        b = result;
    }
    return result;
}

function fibonacciRecursive(num) {
    if (num < 0) {
        throw new Error('Csak pozítiv számmal');
    }
    if (num >= 0 && num < 2) {
        return num
    }
    return fibonacciRecursive(num-1) + fibonacciRecursive(num-2);   
}

let fibonacciArray = [];
for (let i = 0; i < 10; i++) {
    fibonacciArray.push(fibonacci(i));
}

console.log(fibonacciArray);

console.log(fibonacci(5));
console.log(fibonacciRecursive(5));

function* fibonacciGenerator() {
    yield fn1 = 1;
    yield fn2 = 1;
    while (true) {
        let current = fn1 + fn2;
        fn1 = fn2;
        fn2 = current;
        yield current;
    }
}

let it = fibonacciGenerator();
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());

let area = [
    [1, 2, 3, 10],
    [4, 5, 6, 11],
    [7, 8, 9, 12],
    [13, 14, 15, 16]
]

function* colGenerator(array, colIndex) {
    let rowIndex = 0;
    while (rowIndex < array.length) {
        yield array[rowIndex++][colIndex];
    }
}

function* allColGenerator(array) {
    let colIndex = 0
    while (colIndex < array.length) {
        yield colGenerator(array, colIndex++);
    }
}

function* diagonal(array) {
    let colIndex = 0;
    let rowIndex = 0;
    while (rowIndex <  array.length) {
        yield array[rowIndex++][colIndex++];
    }
}

function* diagonal2(array) {
    let colIndex = array.length;
    let rowIndex = 0;
    while (rowIndex <  array.length) {
        yield array[rowIndex++][--colIndex];
    }
}


for (let col of allColGenerator(area)) {
    for (let value of col) {
        console.log('AllCol:', value);
    }
}
for (let col of colGenerator(area, 1)) {
    console.log('Col item:', col);
}

for (let item of diagonal(area)) {
    console.log('Diagonal item:', item);
}


for (let item of diagonal2(area)) {
    console.log('Diagonal item:', item);
}