
function Task(content, done) {
    this.content = content;
    this.done = done;
}

function Feladat() {
}

Task.prototype.markDone = function() {
    this.done = true;
    console.log("markDone runs in prototype");
};

Feladat.prototype.hello = "hello";


let task1 = new Task("levinni a szemetet", false);
let task2 = new Task("ITMP eloadast tartani", false);

console.log(task1);
console.log(task2);

task1.markDone = function() {
    this.done = false;
    console.log("markDone override runs");
}
console.log("task.markDone override");
console.log(task1);
console.log(task2);

task1.markDone();
task2.markDone();

console.log(task1);
console.log("===============")
console.log(task1.__proto__);
console.log(task2.__proto__);
console.log(Object.getPrototypeOf(task1));
console.log(task1.__proto__ === Object.getPrototypeOf(task1));
console.log(Object.getPrototypeOf(task1) === Object.getPrototypeOf(task2));

try {
    task1.print();
} catch (err) {
    console.log("print nincs...");
}

Task.prototype.print = function() {
    console.log(`${this.content} -> ${this.done}` );
}

task1.print();

let tomb = [1,2,3,4,5];

Array.prototype.sum = function() {
    let osszeg = 0;
    this.forEach(element => {
        osszeg += element;
    });
    return osszeg;
}

console.log([1,3,4,5].__proto__.sum);

console.log([1,3,5].sum());

console.log(tomb.sum());